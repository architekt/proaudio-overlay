# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="DSP library for digital signal processing"
HOMEPAGE="https://github.com/sadko4u/lsp-dsp-lib/"
SRC_URI="https://github.com/sadko4u/lsp-dsp-lib/releases/download/${PV}/${PN}-lib-${PV}-src.tar.gz"
LICENSE="LGPL-3+ "
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}/${PN}-lib"

src_configure() {
    emake PREFIX=/usr LIBDIR=/usr/$(get_libdir) config
}
