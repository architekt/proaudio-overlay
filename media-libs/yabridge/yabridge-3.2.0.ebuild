# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson

DESCRIPTION="Yet Another VST bridge, run Windows VST2 plugins under Linux (64-bit VST plugins support only)"
HOMEPAGE="https://github.com/robbert-vdh/yabridge"
SRC_URI="https://github.com/robbert-vdh/${PN}/archive/${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"

DEPEND="
    virtual/wine
    dev-cpp/tomlplusplus
    dev-libs/boost
    dev-libs/bitsery
    dev-libs/function2
    x11-libs/libxcb
"
RDEPEND="${DEPEND}
    !media-libs/yabridge-bin"

src_configure() {
    local emesonargs=(
        --cross-file "${FILESDIR}"/cross-wine64
        -Dwith-vst3=false
    )
    meson_src_configure
}

src_install() {
    dobin "${WORKDIR}/${PN}-${PV}-build/yabridge-host.exe"
    dobin "${WORKDIR}/${PN}-${PV}-build/yabridge-host.exe.so"

    insinto /usr/$(get_libdir)/yabridge
    doins "${WORKDIR}/${PN}-${PV}-build/libyabridge.so"

    use doc && dodoc README.md
}
