# Copyright 2019-2020 Architekt Author
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="RtMidi provides a common C++ API for realtime MIDI input output"
HOMEPAGE="http://www.music.mcgill.ca/~gary/rtmidi/"
SRC_URI="https://dl.bintray.com/aeon-engine/aeon_dependencies/rtmidi/src/rtmidi-4.0.0.tar.gz"

LICENSE="Gary P. Scavone"
SLOT="0/5"
KEYWORDS="~amd64"
IUSE="+alsa doc jack static test"
REQUIRED_USE="|| ( alsa jack )"

DEPEND="alsa? ( media-libs/alsa-lib )
        jack? (
            media-libs/alsa-lib
            virtual/jack
        )"

RDEPEND="${DEPEND}
        doc? ( app-doc/doxygen )"

RESTRICT="!test? ( test )"

src_prepare() {
    default

    # fix wrong libdir
    sed -i "s|\/lib|\/$(get_libdir)|g" rtmidi.pc.in || die

    eautoreconf
}

src_configure() {
    local myconf=(
        --prefix="/usr"
        $(use_with jack)
        $(use_with alsa)
        $(use_enable static)
    )
    econf "${myconf[@]}"
}

src_test() {
    emake check
}

src_install() {
    emake DESTDIR="${D}" install
    find "${D}" -name '*.la' -delete || die

    if use doc ; then
        HTML_DOCS=( doc/html/. )
        einstalldocs
    fi
}
