# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_PN="${PN/-bin/}"

DESCRIPTION="Yet Another VST bridge, run windows VST2/VST3 plugins under Linux (64-bits)"
HOMEPAGE="https://github.com/robbert-vdh/yabridge"
SRC_URI="https://github.com/robbert-vdh/${MY_PN}/releases/download/${PV}/${MY_PN}-${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"
RESTRICT="strip"

DEPEND="
   virtual/wine
   x11-libs/libxcb
"

RDEPEND="${DEPEND}
   !media-libs/yabridge
"

QA_PREBUILT="
   opt/${MY_PN}-${PV}/bin/yabridge-*
   opt/${MY_PN}-${PV}/lib/libyabridge-*
"

S="${WORKDIR}/${MY_PN}"

src_install() {
   into /opt/${MY_PN}-${PV}
   dobin "${WORKDIR}/${MY_PN}/yabridge-host.exe"
   dobin "${WORKDIR}/${MY_PN}/yabridge-host.exe.so"
   dobin "${WORKDIR}/${MY_PN}/yabridge-host-32.exe"
   dobin "${WORKDIR}/${MY_PN}/yabridge-host-32.exe.so"
   dobin "${WORKDIR}/${MY_PN}/yabridgectl"

   insinto /opt/${MY_PN}-${PV}/$(get_libdir)
   doins "${WORKDIR}/${MY_PN}/libyabridge-chainloader-clap.so"
   doins "${WORKDIR}/${MY_PN}/libyabridge-chainloader-vst2.so"
   doins "${WORKDIR}/${MY_PN}/libyabridge-chainloader-vst3.so"
   doins "${WORKDIR}/${MY_PN}/libyabridge-clap.so"
   doins "${WORKDIR}/${MY_PN}/libyabridge-vst2.so"
   doins "${WORKDIR}/${MY_PN}/libyabridge-vst3.so"

   dodir /usr/bin
   dosym /opt/${MY_PN}-${PV}/bin/yabridgectl /usr/bin/yabridgectl
   dosym /opt/${MY_PN}-${PV}/bin/yabridge-host.exe /usr/bin/yabridge-host.exe
   dosym /opt/${MY_PN}-${PV}/bin/yabridge-host.exe.so /usr/bin/yabridge-host.exe.so
   dosym /opt/${MY_PN}-${PV}/bin/yabridge-host-32.exe /usr/bin/yabridge-host-32.exe
   dosym /opt/${MY_PN}-${PV}/bin/yabridge-host-32.exe.so /usr/bin/yabridge-host-32.exe.so
   dosym /opt/${MY_PN}-${PV}/$(get_libdir)/libyabridge-clap.so /usr/$(get_libdir)/libyabridge-clap.so
   dosym /opt/${MY_PN}-${PV}/$(get_libdir)/libyabridge-vst2.so /usr/$(get_libdir)/libyabridge-vst2.so
   dosym /opt/${MY_PN}-${PV}/$(get_libdir)/libyabridge-vst3.so /usr/$(get_libdir)/libyabridge-vst3.so

   if use doc ; then
       insinto /opt/${MY_PN}-${PV}/share/doc/${MY_PN}
       doins CHANGELOG.md README.md
   fi
}

pkg_postinst() {

   einfo "Yabridgectl need to know where to find:"
   einfo "libyabridge-chainloader-clap.so"
   einfo "libyabridge-chainloader-vst2.so"
   einfo "libyabridge-chainloader-vst3.so"
   einfo " "
   einfo "As non-privileged user run:"
   einfo "yabridgectl set --path=/opt/yabridge-${PV}/$(get_libdir)/"
}
