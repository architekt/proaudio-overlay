# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MULTILIB_COMPAT=( abi_x86_{32,64} )
inherit multilib multilib-build unpacker

MYASIOSDK="2.3.3_2019-06-14"
DESCRIPTION="ASIO driver for WINE"
HOMEPAGE="https://github.com/wineasio/wineasio"
SRC_URI="https://github.com/wineasio/wineasio/releases/download/v${PV}/${P}.tar.gz
         https://download.steinberg.net/sdk_downloads/asiosdk_${MYASIOSDK}.zip"
KEYWORDS="~amd64"
LICENSE="GPL-2 STEINBERG-EULA"
IUSE=""
SLOT="0"

DEPEND="virtual/wine:="
RDEPEND="dev-python/PyQt5
         virtual/jack[${MULTILIB_USEDEP}]"

pkg_setup() {
   export WINETARGET=`eselect --brief --colour=no wine show | awk '{$1=$1;print}'`
   [[ $WINETARGET == "(unset)" ]] && die "please set wine version (eselect wine)"
}

src_unpack() {
   unpack ${PN}-${PV}.tar.gz
   unpack_zip asiosdk_${MYASIOSDK}.zip
}

src_prepare() {
   eapply_user
   eapply "${FILESDIR}/gentoo-winetarget.patch"
   cp "${WORKDIR}/asiosdk_${MYASIOSDK}/common/asio.h" .
   cp "${FILESDIR}/wineasio-register" .
   sed -i "s/WINETARGET=/WINETARGET='"${WINETARGET}"'/" wineasio-register
   sed -i "s/WINE64=/WINE64='"${WINETARGET/wine/wine64}"'/" wineasio-register
   sed -i "s/WINE=/WINE='"${WINETARGET}"'/" wineasio-register
   sed -i "s?X-PREFIX-X?\/usr?" gui/wineasio-settings
}

src_compile() {
   use abi_x86_64 && emake 64
   use abi_x86_32 && emake 32
}

src_install() {
   dobin ${S}/wineasio-register
   dobin ${S}/gui/wineasio-settings

   if use abi_x86_64 ; then
      insinto /usr/lib/${WINETARGET}/wine/x86_64-unix
      doins ${S}/build64/wineasio64.dll.so
      insinto /usr/lib/${WINETARGET}/wine/x86_64-windows
      doins ${S}/build64/wineasio64.dll
   fi
   if use abi_x86_32 ; then
      insinto /usr/lib/${WINETARGET}/wine/i386-unix
      doins ${S}/build32/wineasio32.dll.so
      insinto /usr/lib/${WINETARGET}/wine/i386-windows
      doins ${S}/build32/wineasio32.dll
   fi

   dodir /usr/share/${PN} && insinto /usr/share/${PN}
   doins ${S}/gui/*.py
}

pkg_postinst() {
	echo
	elog "Run 'wineasio-register' to register the WineASIO driver in the default Wine prefix"
	elog "You can specify another prefix using the WINEPREFIX environment variable like so:"
	elog "env WINEPREFIX=~/.myprefix wineasio-register"
	echo
}
