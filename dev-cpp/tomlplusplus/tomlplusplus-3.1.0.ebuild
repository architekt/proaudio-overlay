# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson

DESCRIPTION="Header-only TOML config file parser and serializer for modern C++"
HOMEPAGE="https://github.com/marzer/tomlplusplus"
SRC_URI="https://github.com/marzer/${PN}/archive/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND=""
DEPEND="${RDEPEND}"

src_configure() {
    meson_src_configure
}

src_install() {
    meson_src_install
}
