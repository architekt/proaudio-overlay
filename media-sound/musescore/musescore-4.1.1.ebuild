# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake qmake-utils unpacker xdg

CMAKE_MAKEFILE_GENERATOR="emake"
MY_VSTSDK="3.7.8"

DESCRIPTION="WYSIWYG music notation"
HOMEPAGE="https://musescore.org/"
SRC_URI="https://github.com/musescore/MuseScore/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
         vst3? ( https://framagit.org/syntazia/vst3sdk/-/archive/3.7.8/vst3sdk-${MY_VSTSDK}.tar.gz )"

KEYWORDS="~amd64 ~x86 ~arm64 ~arm"
LICENSE="GPL-2 vst3? ( GPL-3 )"
SLOT="0"
IUSE="doc +jumbo-build test +vst3"

RESTRICT="!test? ( test )"

BDEPEND="
   test? ( dev-cpp/gtest )
   dev-qt/linguist-tools:5
   virtual/pkgconfig"
DEPEND="
   dev-libs/tinyxml2:=
   dev-qt/designer:5
   dev-qt/qtconcurrent:5
   dev-qt/qtcore:5
   dev-qt/qtdeclarative:5
   dev-qt/qtgui:5
   dev-qt/qthelp:5
   dev-qt/qtnetwork:5
   dev-qt/qtnetworkauth:5
   dev-qt/qtopengl:5
   dev-qt/qtprintsupport:5
   dev-qt/qtquickcontrols2:5
   >=dev-qt/qtsingleapplication-2.6.1_p20171024[X]
   dev-qt/qtsvg:5
   dev-qt/qtxml:5
   dev-qt/qtxmlpatterns:5
   >=media-libs/freetype-2.5.2
   >=media-libs/alsa-lib-1.0.0
   media-libs/flac
   media-sound/lame
   media-libs/libopusenc
   media-libs/libvorbis
   vst3? ( virtual/wine )"
RDEPEND="${DEPEND}"

S="${WORKDIR}/MuseScore-${PV}"

PATCHES=(
   "${FILESDIR}/0001-use-system-libs.patch"
   "${FILESDIR}/0002-avoid-compressing-manpage.patch"
)

src_prepare() {
   cmake_src_prepare
   use vst3 && PATCHES+=( "${FILESDIR}/0003-enable-vst.patch" )
   rm -r thirdparty/{flac,freetype,lame,opus,opusenc} || die
}

src_configure() {
   export PATH="$(qt5_get_bindir):${PATH}"
   CMAKE_BUILD_TYPE="Release"

   local mycmakeargs=(
      -DCMAKE_BUILD_TYPE="Release"
      -DMUSESCORE_BUILD_MODE="release"
      -DCMAKE_C_FLAGS_RELEASE="$CFLAGS"
      -DCMAKE_CXX_FLAGS_RELEASE="$CXXFLAGS"
      -DMUE_BUILD_CRASHPAD_CLIENT=OFF
      -DMUE_BUILD_UNIT_TESTS="$(usex test)"
      -DMUE_COMPILE_USE_UNITY="$(usex jumbo-build)"
      -DMUE_COMPILE_USE_CCACHE=OFF
      -DMUE_DOWNLOAD_SOUNDFONT=OFF
      -DMUE_ENABLE_LOGGER_DEBUGLEVEL=OFF
      -DMUE_ENABLE_DRAW_TRACE=OFF
      -DMUE_CRASH_REPORT_URL=""
      -DUSE_SYSTEM_LIBS=ON
      )

   if use vst3; then
      mycmakeargs+=(
         -DMUE_BUILD_VST_MODULE=ON
         -DVST3_SDK_PATH="${WORKDIR}/vst3sdk-${MY_VSTSDK}"
      )
   fi

   cmake_src_configure

   ## BUILD_AUTOBOT_MODULE
   ## The AutoBot module contains some functionality that is
   ## mainly used internally for some automated tests.
   ## That option lets you decide whether to build it or not.
   ## Several modules have such an option to disable them;
   ## for some modules this makes more sense than for others though,
   ## and in practice nobody ever uses these options,
   ## so the build might fail with them disabled.
}

src_test() {
   virtx cmake_src_test
}

src_install() {
   use doc && dodoc doc/*.md
   cmake_src_install
}
