# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 meson gnome2-utils xdg-utils

DESCRIPTION="A highly automated and intuitive digital audio workstation"
HOMEPAGE="https://www.zrythm.org/en/"

EGIT_REPO_URI="https://github.com/${PN}/${PN}.git"
EGIT_BRANCH="master"

LICENSE="AGPL-3"
SLOT="0"

IUSE="carla doc doc-html dsp graphviz jack man portaudio profiling pulseaudio rtaudio rtmidi sdl sndio test"
REQUIRED_USE="|| ( pulseaudio sndio )"

LANGS="de el en en-GB es fr gl it ja nb pl pt pt-BR ru zh"
for lang in ${LANGS} ; do
    IUSE+=" l10n_${lang}"
done

## Set english to default
IUSE="${IUSE/l10n_en/+l10n_en}"

RESTRICT="!test? ( test )"
RESTRICT+=" strip"

RDEPEND="
    x11-libs/gtk+:3
    x11-libs/gtksourceview
    jack? ( virtual/jack )
    rtaudio? ( media-libs/rtaudio[alsa] )
    rtmidi? ( media-libs/rtmidi[alsa] )
    portaudio? ( media-libs/portaudio )
    pulseaudio? ( media-sound/pulseaudio )
    sndio? ( media-libs/libsoundio )
    media-fonts/dseg-fonts
    media-libs/alsa-lib
    media-libs/libsoundio
    media-libs/chromaprint
    sdl? ( media-libs/libsdl2 )
    media-libs/libsndfile
    media-libs/lv2
    media-libs/lilv
    media-libs/rubberband
    sdl? ( media-libs/libsdl2 )
    carla? ( >=media-sound/carla-2.3_alpha2 )
    gnome-base/libgtop
    dev-libs/libcyaml
    dev-libs/libaudec
    dev-libs/libpcre
    dev-libs/reproc
    dev-scheme/guile
    dsp? ( media-libs/lsp-dsp )
    graphviz? ( media-gfx/graphviz )
    sci-libs/fftw:*[threads]
"

DEPEND="${RDEPEND}
    doc? ( dev-python/sphinx )"

PATCHES=("${FILESDIR}/0001-fix-undefined-reference.patch")

src_configure() {
    export GUILE_AUTO_COMPILE=0

    local emesonargs=(
        $(meson_use profiling )
        $(meson_use test tests)
        $(meson_use test gui_tests)
        $(meson_feature portaudio)
        $(meson_feature rtmidi )
        $(meson_feature rtaudio )
        $(meson_feature sdl )
        $(meson_feature carla )
        $(meson_feature jack )
        $(meson_use man manpage )
        $(meson_use doc user_manual )
        -D graphviz=$(usex graphviz enabled disabled )
        -D guile=enabled
        -D lsp_dsp=$(usex dsp enabled disabled )
        -D dseg_font=false
    )
    meson_src_configure
}

src_install() {
    meson_src_install
}

pkg_preinst() {
    gnome2_gdk_pixbuf_savelist
}

pkg_postinst() {
    gnome2_schemas_update
    gnome2_gdk_pixbuf_update
    xdg_icon_cache_update
    xdg_desktop_database_update
    xdg_mimeinfo_database_update
}

pkg_postrm() {
    gnome2_schemas_update
    gnome2_gdk_pixbuf_update
    xdg_icon_cache_update
    xdg_desktop_database_update
    xdg_mimeinfo_database_update
}
