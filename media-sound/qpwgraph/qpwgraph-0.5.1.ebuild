# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake xdg

DESCRIPTION="A PipeWire Graph Qt GUI Interface"
HOMEPAGE="https://gitlab.freedesktop.org/rncbc/qpwgraph"

KEYWORDS="~amd64 ~x86"
SRC_URI="https://gitlab.freedesktop.org/rncbc/qpwgraph/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"

LICENSE="GPL-2+"
SLOT="0"
IUSE="wayland"

S="${WORKDIR}/${PN}-v${PV}"

DEPEND="
   dev-qt/qtcore:5
   dev-qt/qtgui:5
   dev-qt/qtwidgets:5
   dev-qt/qtxml:5
   media-video/pipewire
   dev-qt/qtnetwork:5"
RDEPEND="${DEPEND}"

src_configure() {
   CMAKE_BUILD_TYPE="Release"

   local mycmakeargs=(
      -DCONFIG_ALSA_MIDI=YES
      -DCONFIG_SYSTEM_TRAY=YES
      -DCONFIG_WAYLAND=$(usex wayland)
      -DCONFIG_QT6=NO
   )
   cmake_src_configure
}
