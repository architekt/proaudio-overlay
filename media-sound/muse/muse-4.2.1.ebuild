# Copyright 2019-2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake unpacker xdg

MYSDK="3.7.8"
DESCRIPTION="MusE is a Digital Audio Workstation (DAW)"
HOMEPAGE="https://muse-sequencer.github.io/"
SRC_URI="https://github.com/muse-sequencer/${PN}/releases/download/${PV}/${P}.tar.gz
         vst? ( https://framagit.org/syntazia/vst3sdk/-/archive/${MYSDK}/vst3sdk-${MYSDK}.tar.gz )"

LICENSE="GPL-2 vst? ( GPL-3 )"
SLOT="0"
KEYWORDS="~amd64"
IUSE="dssi debug extra fluidsynth lash lrdf lv2 osc rtaudio rubberband +vst"

BDEPEND="
   virtual/pkgconfig"
DEPEND="
   dev-qt/qtcore:5
   dev-qt/qtgui:5
   dev-qt/qtwidgets:5
   dev-qt/qtxml:5
   dev-qt/qtx11extras:5
   virtual/jack
   media-libs/ladspa-sdk
   media-libs/libsamplerate
   media-libs/libsndfile
   dssi? ( media-libs/dssi )
   extra? ( media-libs/libinstpatch )
   fluidsynth? ( media-sound/fluidsynth )
   lash? ( media-sound/lash )
   lrdf? ( media-libs/liblrdf )
   lv2? ( dev-libs/sord
       media-libs/lilv
       media-libs/lv2 )
   osc? ( media-libs/liblo )
   rtaudio? ( media-libs/rtaudio )
   rubberband? ( media-libs/rubberband )
   vst? ( virtual/wine )"
RDEPEND="${DEPEND}"

src_unpack() {
   unpack ${PN}-${PV}.tar.gz
   use vst && unpack vst3sdk-${MYSDK}.tar.gz
}

src_prepare() {
   cmake_src_prepare
}

src_configure() {

   local CMAKE_BUILD_TYPE="Release"
   use debug && local CMAKE_BUILD_TYPE="Debug"

   local mycmakeargs=(
      -DLIB_INSTALL_DIR="${EPREFIX}/usr/$(get_libdir)"
      -DENABLE_DSSI=$(usex dssi ON OFF)
      -DENABLE_FLUID=$(usex fluidsynth ON OFF)
      -DENABLE_INSTPATCH=$(usex extra ON OFF)
      -DENABLE_LASH=$(usex lash ON OFF)
      -DENABLE_LRDF=$(usex lrdf ON OFF)
      -DENABLE_LV2=$(usex lv2 ON OFF)
      -DENABLE_LV2_GTK2=OFF
      -DENABLE_MIDNAM=ON
      -DENABLE_OSC=$(usex osc ON OFF)
      -DENABLE_RTAUDIO=$(usex rtaudio ON OFF)
      -DENABLE_RUBBERBAND=$(usex rubberband ON OFF)
      -DENABLE_VST_NATIVE=OFF
      -DVST_HEADER_PATH="${WORKDIR}/vst3sdk-${MYSDK}"
      -DENABLE_VST_VESTIGE=OFF
   )

   cmake_src_configure
}
