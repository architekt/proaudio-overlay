# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font

DESCRIPTION="7-segment and 14-segment font"
HOMEPAGE="https://www.keshikan.net/fonts-e.html/"
SRC_URI="https://github.com/keshikan/DSEG/releases/download/v${PV}/fonts-DSEG_v${PV//.}.zip -> ${PN}-${PV}.zip"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"


S="${WORKDIR}/fonts-DSEG_v046"

FONT_SUFFIX="ttf"
FONT_S=(
    "${S}"/DSEG14-Classic
    "${S}"/DSEG14-Classic-MINI
    "${S}"/DSEG14-Modern
    "${S}"/DSEG14-Classic-MINI
    "${S}"/DSEG14-Modern-MINI
    "${S}"/DSEG7-Classic
    "${S}"/DSEG7-Modern
    "${S}"/DSEGWeather
    "${S}"/DSEG14-Classic
    "${S}"/DSEG14-Modern
    "${S}"/DSEG7-7SEGG-CHAN
    "${S}"/DSEG7-Classic-MINI
    "${S}"/DSEG7-Modern-MINI
)

#DOCS=""

src_install() {
    font_src_install 
    use doc && dodoc README.md
}
