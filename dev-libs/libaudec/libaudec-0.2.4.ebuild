# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson

DESCRIPTION="A library for reading and resampling audio files"
HOMEPAGE="https://github.com/zrythm/libaudec"
SRC_URI="https://github.com/zrythm/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc test"
RESTRICT="!test? ( test )"

RDEPEND="
        media-libs/libsamplerate
	media-libs/libsndfile
"
DEPEND="${RDEPEND}"

src_configure() {
    local emesonargs=(
        $(meson_use test tests)
    )
    meson_src_configure
}

src_install() {
    DESTDIR="${D}" eninja -C "${BUILD_DIR}" install
    use doc && einstalldocs
}
