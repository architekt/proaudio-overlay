# Copyright 2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="Provides improved implementations of std::function:"
HOMEPAGE="https://github.com/Naios/function2/"
SRC_URI="https://github.com/Naios/function2/archive/${PV}.tar.gz  -> ${P}.tar.gz"
KEYWORDS="~amd64"
IUSE="test"
RESTRICT="!test? ( test )"

LICENSE="Boost-1.0"
SLOT="0"

RDEPEND=""
DEPEND="${RDEPEND}"

PATCHES=(
    "${FILESDIR}/0002-create-install-pkgconfig-file.patch"
    "${FILESDIR}/0003-function2-pc.patch"
)

src_configure() {
    local mycmakeargs=(
    -DBUILD_TESTING=$(usex test ON OFF)
    )
    cmake_src_configure
}

src_install() {
    cmake_src_install
    rm -rf "${D}/usr/Readme.md"
    rm -rf "${D}/usr/LICENSE.txt"
}
