# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C library for reading and writing YAML"
HOMEPAGE="https://github.com/tlsa/libcyaml"
SRC_URI="https://github.com/tlsa/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc doc-html test"
RESTRICT="!test? ( test )"

RDEPEND="dev-libs/libyaml"
DEPEND="${RDEPEND}
	doc-html? ( app-doc/doxygen )"

src_compile() {
    emake VARIANT=release
    use doc-html && emake docs
}

src_test() {
    emake test
}

src_install() {
    emake DESTDIR="${D}" PREFIX="/usr" LIBDIR="$(get_libdir)" VARIANT=release install

    use doc && dodoc README.md CHANGES.md

    if use doc-html ; then
        docinto html/api
        dodoc -r build/docs/api/html/.
        docinto html/devel
        dodoc -r build/docs/devel/html/.
    fi
}
