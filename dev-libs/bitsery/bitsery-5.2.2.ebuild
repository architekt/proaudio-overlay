# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

DESCRIPTION="Your binary serialization library"
HOMEPAGE="https://github.com/fraillt/bitsery"
SRC_URI="https://github.com/fraillt/${PN}/archive/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"

IUSE="doc examples test"
RESTRICT="!test? ( test )"

BDEPEND="sys-devel/gcc"

PATCHES=(
    "${FILESDIR}/0001-generate-pkgconfig-v522.patch"
    "${FILESDIR}/0002-bitsery-pc.patch"
)

src_configure() {
    local mycmakeargs=(
        -DBITSERY_BUILD_EXAMPLES=$(usex examples)
        -DBITSERY_BUILD_TESTS=$(usex test)
    )
    cmake-utils_src_configure
}

src_install() {
    cmake-utils_src_install
}
