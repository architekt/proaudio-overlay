# Copyright 2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="A library for dealing with external processes"
HOMEPAGE="https://github.com/DaanDeMeyer/reproc/"
SRC_URI="https://github.com/DaanDeMeyer/reproc/archive/v${PV}.tar.gz  -> ${P}.tar.gz"
KEYWORDS="~amd64"

LICENSE="MIT"
SLOT="0"

RDEPEND=""
DEPEND="${RDEPEND}"
